import click
import aiohttp
import asyncio
import os
import json
import time
from dataclasses import dataclass, field
from typing import List
import csv
import math

@dataclass
class PayloadFromRequest:
    id: str = None
    attack_vector: str = None


@dataclass
class PayloadData:
    data: List[PayloadFromRequest] = field(default_factory=list)


def write_payload_into_csv(data: PayloadData):
    with open('payload.csv', 'w', newline='') as csv_file:
        fieldnames = ['id', 'attack_vector']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for payload in data.data:
            writer.writerow({'id': payload.id, 'attack_vector': payload.attack_vector})


# a = PayloadData()
# l = PayloadFromRequest(4,attack_vector= "dddd")
# p = PayloadFromRequest(5,"sfsf")
# a.data.append(l)
# a.data.append(p)
# write_payload_into_csv(a)

@click.command()
@click.option("--typ", required=True, help="CPE type.")
@click.option("--product", default="*", help="CPE product.")
@click.option("--vendor", default="*", help="CPE vendor.")
@click.option("--version", default="*", help="CPE version.")
def search(typ, product, vendor, version):
    url = "https://services.nvd.nist.gov/rest/json/cves/1.0?cpeMatchString=cpe:2.3:"

    if typ[0] == "a":
        url += "a"
    elif typ[0] == "h":
        url += "h"
    elif typ[0] == "o":
        url += "o"
    else:
        raise typ

    url += ":" + product + ":" + vendor + ":" + version

    async def req(index=0):
        print("Requesting url", index)
        print(url)
        async with aiohttp.ClientSession() as session:
            async with session.get(
                url + "&startIndex=" + str(index), proxy=os.getenv("HTTP_PROXY", None)
            ) as response:
                print("url: ", url + "&startIndex=" + str(index))
                print("Status: ", response.status)
                print("Content-type: ", response.headers["content-type"], "\n\n\n")
                if (
                    response.headers["content-type"] == "text/html"
                    and response.status == 403
                ):
                    # Тут можно поиграть со временем дабы пощупать ограничения по R.P.S R.P.M (R.P.H, если такой есть)
                    await asyncio.sleep(60)
                    return await req(index)
                else:
                    html = await response.text()
                    return html

    async def main():
        total = totalResults(await req(0))

        payload_data = PayloadData()
        pages = math.ceil(int(total / 100))  # Тут можно задать константу, если выдача большая, например ставить = 10

        print("total pages", pages)

        tasks = []
        count = 0
        for i in range(0, int(pages / 3)):
            for j in range(0, 3):
                print(f"Request, page #{count+1}")
                tasks.append(req(count * 100))
                count += 1
            #time.sleep(0.3)

        res = await asyncio.gather(*tasks)

        for i in range(0, count):
            print(f"Response, page #{i+1}")
            handle(res[i], payload_data)
        write_payload_into_csv(payload_data)

    asyncio.get_event_loop().run_until_complete(main())



def totalResults(text):
    if text == "ERROR":
        print("Error 403, pass")
        pass
    else:
        data = json.loads(text)
        return data["totalResults"]


def handle(text, payload_data:PayloadData):

    #print("handle", len(text))
    if text == "ERROR":
        print("Error 403, pass")
        payload_data.data.append(PayloadFromRequest("ERROR", "403"))
    else:
        try:
            data = json.loads(text)

            for cve in data["result"]["CVE_Items"]:
                try:
                    id = cve["cve"]["CVE_data_meta"]["ID"]
                    attack_vector = cve["impact"]["baseMetricV3"]["cvssV3"]["attackVector"]
                    payload_data.data.append(PayloadFromRequest(id, attack_vector))

                except KeyError:
                    pass
        finally:
            print("added")


if __name__ == "__main__":
    search()
