import argparse
import json
import tempfile

'''
class Storage:
    def __init__(self, _file_path):
        self.file_path = _file_path
'''



def get_args():
    parser = argparse.ArgumentParser(description="key-value storage")
    parser.add_argument(
        '--key',
        type=str,
        help='provide key name in json storage'
    )
    parser.add_argument(
        '--value',
        default=None,
        help='add value in json storage by key'
    )
    args = parser.parse_args()
    return args


def load_storage(path):
    with open(path, "r") as fp:
        data = json.load(fp)
    return data


def save_storage(data, path):
    with open(path, "w") as fp:
        json.dump(data, fp)


def args_handler(args):
    file_path = 'tmp.json'
    try:
        data = load_storage(file_path)
    except Exception as e:
        print(f'there is no storage with path such as {file_path}')
        if args.value is not None:
            print("creating the storage file...")
            data = dict()
        else:
            return
    try:
        if args.value is None:
            '''получить значение по ключу'''
            print(data[args.key])
        else:
            '''иначе добавляем в хранилище'''
            if args.key in data.keys():
                data[args.key].append(args.value)
            else:
                data[args.key] = [args.value]
            print(f'successfully added value({args.value}) on key({args.key}) in storage {file_path}')
        save_storage(data, file_path)

    except Exception as e:
        print(e)

if __name__ == "__main__":
    args_handler(get_args())
