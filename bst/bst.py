import itertools


class Node:
    def __init__(self, value = None):
        self.left_child = None
        self.right_child = None
        self.value = value


class BinarySearchTree:

    def __init__(self, value=None):
        self.root = Node(value)

    def add_node(self, value):
        if self.root.value is None:
            self.root.value = value

        elif self.root.value <= value:
            if self.root.left_child is None:
                self.root.left_child = BinarySearchTree(value)

            else:
                self.root.left_child.add_node(value)

        elif self.root.value > value:
            if self.root.right_child is None:
                self.root.right_child = BinarySearchTree(value)
            else:
                self.root.right_child.add_node(value)

    def __iter__(self):

        if self.root.right_child:
            yield from self.root.right_child.__iter__()
        yield self.root.value
        if self.root.left_child:
            yield from self.root.left_child.__iter__()


tree = BinarySearchTree()
elements_to_add = [20,16,25,6,17,21,29,0,7,28,51,46]

for node in elements_to_add:
    tree.add_node(node)

for i in tree:
    print(i)




