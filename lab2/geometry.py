from math import sin, radians, pi  # Математические приколы
import abc  # для абстрактных классов


class Figure(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def square(self):
        pass


class Rectangle(Figure):
    __name__ = "Rectangle"

    def __init__(self, __a, __b):
        self.__a = __a
        self.__b = __b

    def square(self):
        return self.__a * self.__b


class Triangle(Figure):
    __name__ = "Triangle"

    def __init__(self, __a, __b, __alpha):
        self.__a = __a
        self.__b = __b
        self.__alpha = radians(__alpha)

    def square(self):
        return abs(1 / 2 * self.__a * self.__b * sin(self.__alpha))


class Circle(Figure):
    __name__ = "Circle"

    def __init__(self, __radius):
        self.__radius = __radius

    def square(self):
        return pi * self.__radius ** 2


if __name__ == "__main__":
    figure_arr = [Rectangle(5, 3), Triangle(3, 7, 30), Circle(5)]
    for figure in figure_arr:
        print(f"figure {figure.__name__}, S = {figure.square()}")
