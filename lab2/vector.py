from math import sqrt


class Vector(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"<{self.x};{self.y}>"

    def __add__(self, other):
        if type(self) != type(other):
            return None
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        if isinstance(other, Vector):
            return self.x * other.x + self.y * other.y
        elif isinstance(other, int):
            return Vector(self.x * other, self.y * other)
        return None

    def __rmul__(self, other):
        return self * other

    def __eq__(self, other):
        if not isinstance(other, Vector):
            return None
        return self.x == other.x and self.y == other.y

    def __gt__(self, other):
        if isinstance(other, Vector):
            return self.x > other.x and self.y > other.y
        return None

    def __ge__(self, other):
        return self > other or self == other

    def __abs__(self):
        return sqrt(self.x ** 2 + self.y**2)

    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        return self.__str__()


if __name__ == "__main__":

    v = Vector(1.5, 5.1)  # Конструктор
    b = Vector(2, -1)
    print(v)
    print(v + b)  # Сложение векторов
    print(2 * v)  # Умножение вектора на число
    print(v * b)  # Скалярное произведение векторов
    print(v == b)  # Сравнение
    print(v != b)  # Не равно
    print(v.__abs__())  # Длина вектора
