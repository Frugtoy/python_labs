import time
import timeit

FILE = 'log.txt'


class Timer:

    def __init__(self, func):
        self.func = func

    # decorator
    def __call__(self, arg):
        pref_time = timeit.default_timer()
        start_time = time.asctime()
        result = self.func(arg)
        end_time = timeit.default_timer() - pref_time
        print(f"Current time for execution{self.func.__name__}:{end_time} ms")
        with open(FILE, 'a') as f:
            f.write(f"{start_time}:{self.func.__name__}")
        return [result, end_time,start_time,self.func.__name__]


class HtmlTimer:
    def __init__(self, decor):
        self.decor = decor

    def __call__(self, args):
        data = self.decor.__call__(args)
        with open(f'{data[3]}.html', 'w') as  file:
            file.write(f"<html><body>{data[1]}</body></html>")
        return data[0]


@HtmlTimer
@Timer
def list_cycle_square(arg):
    return [x ** 2 for x in arg]


list = [123,242,12,4,5,2424,121]
print(list_cycle_square(list))

