import time
import functools


def lensort(unsorted):
    unsorted = [1,2,3]
    sorted_ = sorted(unsorted, key=len)
    print(sorted_)


def unique(not_unique):
    print(set(not_unique))


def my_enumirate(listy):
    n = list(zip(listy, range(len(listy))))
    print(n)


def frequency(path):
    with open(path, 'r') as file:
        text = file.read()
        word_raw = text.split()
        dictionary = dict()
        for word in word_raw:
            if word in dictionary.keys():
                dictionary[word] +=1
            else:
                dictionary[word] = 1
        print(dictionary)


def timer(func):
    @functools.wraps(func)
    def _wrapper(*args, **kwargs):
        start_timer = time.perf_counter()
        result = func(*args,**kwargs)
        finished_timer =time.perf_counter() - start_timer
        print(f"{func.__name__} took {finished_timer} seconds")
        return result
    return _wrapper


@timer
def with_for(arr):
    for i in range(0, len(arr)):
        arr[i] **= 2
    print(arr)


@timer
def with_list_compression(arr):
     arr = list([arr[i] **2 for i in range(0, len(arr))])
     print(arr)


@timer
def with_map(arr):
    arr = list(map(lambda x : x**2,arr))
    print(arr)


if __name__ == "__main__":
    print("task1")
    lensort(["fwfegreg","aefeerf","dsd","a"])
    print("task2")
    unique([1,3,1,1,3])
    print("task3")
    my_enumirate(["sdsd","sfsf","sfsf"])
    print("task4")
    frequency("../in")
    print("task5")
    with_for([1,2,5])
    with_list_compression([1,2,5])
    with_map([1,2,5])
    print(with_map.__name__)

